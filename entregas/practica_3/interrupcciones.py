from hardware import *
from so import *

class IoDeviceController():

    def __init__(self, device):
        self._device = device
        self._waiting_queue = []
        self._currentPCB = None

    def runOperation(self, pcb, instruction):
        pair = {'pcb': pcb, 'instruction': instruction}
        self._waiting_queue.append(pair)
        self.__load_from_waiting_queue_if_apply()

    def getFinishedPCB(self):
        finishedPCB = self._currentPCB
        self._currentPCB = None
        self.__load_from_waiting_queue_if_apply()
        return finishedPCB

    def __load_from_waiting_queue_if_apply(self):
        if (len(self._waiting_queue) > 0) and self._device.is_idle:
            pair = self._waiting_queue.pop()
            pcb = pair['pcb']
            instruction = pair['instruction']
            self._currentPCB = pcb
            self._device.execute(instruction)

    def __repr__(self):
        return "IoDeviceController for {deviceID} running: {currentPCB} waiting: {waiting_queue}".format(deviceID=self._device.deviceId, currentPCB=self._currentPCB, waiting_queue=self._waiting_queue)

class AbstractInterruptionHandler():
    def __init__(self, kernel):
        self._kernel = kernel

    @property
    def kernel(self):
        return self._kernel

    def execute(self, irq):
        print('EXECUTE MUST BE OVERRIDEN in class {classname}'.format(classname=self.__class__.__name__))

class CpuIntrInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        self.kernel.modeGannt.save(self.kernel.pcbTable.modeGannt())
        self.kernel.timeLine.save()

class TimeOutInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        if not self.kernel.readyQueue.isEmpty() and self.kernel.pcbTable.isAnyRunning():

            pcbToWait = self.kernel.pcbTable.returnRunningPCB()

            self.kernel.pcbTable.updatePCofPcb(pcbToWait, HARDWARE.cpu.pc)
            self.kernel.pcbTable.changeState(pcbToWait, 'waiting')
            self.kernel.dispatcher.save(pcbToWait)
            self.kernel.readyQueue.addToReady(pcbToWait.pid)

            pcbToLoad = self.kernel.readyQueue.getNextPid()

            self.kernel.pcbTable.changeState(self.kernel.pcbTable.findPcb(pcbToLoad), 'running')
            self.kernel.dispatcher.load(self.kernel.pcbTable.findPcb(pcbToLoad))

        elif HARDWARE.cpu.pc == -1:

            HARDWARE.cpu.pc  = 0