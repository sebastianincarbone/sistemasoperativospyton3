#!/usr/bin/env python
from re import escape

from hardware import *
from console import  *
from interrupcciones import *
from SchedulerNoExpropiativo import *

##Schedulers
    #SchedulerFCFS
    #SchedulerRR
    #SchedulerExpropiativo
    #SchedulerNoExpropiativo
    #SchedullerExpropiativoConRR

from tabulate import tabulate
from termcolor import colored, cprint

class NewPCBTable():

    # El PCBTable tipo {pid, pcb}
    # En esta clase definimos los programas que estan cargados en memoria y le establecemos un estado
    # Metodos establecidos son:
    #   extractFromTable(self, pcb) -> saca pcb del diccionario
    #   isRunning(self, Pid) -> denota el pcb que se esta corriendo mediante un runningPid
    #   addPCBTable(self, pcb) -> agrega un pcb al diccionario
    #   generatePID(self, pcb) -> genera un PID como primaryKey para cada PCB del diccionario
    #   modificarPCBEnTable (pcb, baseDir,limmit,pcActual,estado) -> modifica un elemento de la lista

    def __init__(self):
        self.__listPCB = dict()
        self.__pid = 0
        self.__runningPid = -1

    @property
    def listPCB(self):
        return self.__listPCB

    @property
    def runningPid(self):
        return self.__runningPid

    @runningPid.setter
    def runningPid(self, runningPidNew):
        self.__runningPid = runningPidNew

    @property
    def pid(self):
        return self.__pid

    @pid.setter
    def pid(self, pidNew):
        self.__pid = pidNew

    def isAnyRunning(self):
        return self.runningPid != -1

    def returnRunningPCB(self):
        return self.listPCB[self.runningPid]

    def findPcb(self, pid):
        return self.listPCB[pid]

    def addPcbToTable(self, pcb):
        self.listPCB[pcb.pid] = pcb

    def generatePID(self, pid):
        return pid + 1

    def modifyPcb(self, pcb, pcActual, baseDir, limit, state):
        self.listPCB[pcb.pid] = pcb.modify(pcActual, baseDir, limit, state)

    def updatePCofPcb(self, pcb, pc):
        return pcb.modify(pc, pcb.baseDir, pcb.limit, pcb.state)

    def changeState(self, pcb, state):

        if state == 'running':
            self.runningPid = pcb.pid
        elif state == 'waiting':
            self.runningPid = -1
        elif state == 'finished':
            self.runningPid = -1

        return pcb.modify(pcb.pc, pcb.baseDir, pcb.limit, state)

    def toUpgrade(self):
        retorno = list()
        for k, pcb in self.__listPCB.items():
            if pcb.age >= 3:
                pcb.age = 0
                retorno.append((pcb.age,pcb.lvl))
            else:
                pcb.age = pcb.age + 1

        return retorno

    def print(self):
        retorno = self.listPCB
        if not retorno:
            print('isEmpty')
        else:
            headers = ['Pid', 'pc', 'baseDir', 'limit', 'state']

            data = sorted(
                [(p.pid, p.pc, p.baseDir, p.limit, p.state) for k, p in retorno.items()])

            print(tabulate(data, headers=headers, tablefmt="fancy_grid"))

    def modeGannt(self):
        listaLocal = self.listPCB
        listaProgramaEstado = list()
        if listaLocal:
            for key, pcb in listaLocal.items():
                if   pcb.state == 'ready':
                    prog = key
                    listaProgramaEstado.append('Ready')
                elif pcb.state == 'running':
                    prog = key
                    listaProgramaEstado.append('Run')
                elif pcb.state == 'waiting':
                    prog = key
                    listaProgramaEstado.append('Blocked')
                elif pcb.state == 'finished':
                    prog = key
                    listaProgramaEstado.append('Finished')
            return  listaProgramaEstado
        else:
            return  'nill'

    def __repr__(self):
        retorno = self.listPCB
        if not retorno:
            print('isEmpty')
        else:
            headers = ['Pid', 'pcActual', 'baseDir', 'limit', 'estado']

            data = sorted(
                [(p.pid, p.pc, p.baseDir, p.limmit, p.estado) for k, p in retorno.items()])

            print(tabulate(data, headers=headers, tablefmt="fancy_grid"))

class TimeLine():
    def __init__(self, kernel):
        self.__timeLine = dict()
        self.__tickNumber = 0
        self.__kernel = kernel
        self.__hardware = HARDWARE

    @property
    def timeLine(self):
        return self.__timeLine

    @property
    def tickNumber(self):
        return self.__tickNumber

    @tickNumber.setter
    def tickNumber(self, tickNumber):
        self.__tickNumber = tickNumber

    @property
    def kernel(self):
        return self.__kernel

    @property
    def hardware(self):
        return self.__hardware

    def save(self):
        self.timeLine[self.tickNumber] = Imagen(self.kernel).generate()
        self.tickNumber += 1

    def remove(self, tickNumber):
        self.timeLine.__delitem__(tickNumber)

    def getitem(self, item):
        return self.timeLine[item]

class Imagen():
    def __init__(self, kernel):
        self.__imgLocal = dict()
        self.__kernel = kernel
        self.__hardware = HARDWARE

    @property
    def imgLocal(self):
        return self.__imgLocal

    @property
    def kl(self):
        return self.__kernel

    @property
    def hw(self):
        return self.__hardware

    def generate(self):
        self.imgLocal['pc'] = self.hw.cpu.pc
        self.imgLocal['ir'] = self.hw.cpu._ir
        self.imgLocal['baseDir'] = self.hw.mmu.baseDir
        self.imgLocal['limmit'] = self.hw.mmu.limit

        return self.__imgLocal

class Dispatcher ():
    def __init__(self,kernel):
        self.__cpu = HARDWARE.cpu
        self.__mmu = HARDWARE.mmu
        self.__kernel = kernel

    @property
    def cpu(self):
        return self.__cpu

    @property
    def mmu(self):
        return self.__mmu

    @property
    def kernel(self):
        return self.__kernel

    def load (self,pcb):
        #print('\n+------------Dispatcher.load')

        if pcb.pc == -1:
            self.cpu.pc = 0
        else :
            self.cpu.pc = pcb.pc

        self.mmu.baseDir = pcb.baseDir
        self.mmu.limit   = pcb.limit

    def save (self, pcb):
        #print('\n+------------Dispatcher.save')

        self.kernel.pcbTable.modifyPcb(pcb, self.cpu.pc, self.mmu.baseDir, self.mmu.limit, pcb.state)
        self.cpu.pc = -1

class Console():
    def __init__(self, kernel):
        self.__kernel = kernel

    @property
    def kernel(self):
        return self.__kernel

    def executeComandLine(self, comand):
        CONSOLE.executeCLI(comand, self.kernel)

class ModeGannt():
    def __init__(self):
        self.__tupleList = list()

    @property
    def tupleList(self):
        return self.__tupleList

    def save(self,listaDeProgramaEstado):
        if listaDeProgramaEstado != 'nill':
            self.tupleList.append(listaDeProgramaEstado)

    def print(self):
        return  self.tupleList

class Kernel():

    def __init__(self):

        self.__newPid = 0
        self.__flag = 'run'

        #self._readyQueue    = ReadyQueue()
        self._readyTable    = ReadyTable()

        self._dispatcher    = Dispatcher(self)
        self.modeGannt      = ModeGannt()
        self._timeLine      = TimeLine(self)
        self._pcbTable      = NewPCBTable()
        self._console       = Console(self)
        self._loader        = Loader()

        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        timeOutHandler = TimeOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIME_OUT_INTERRUPTION_TYPE, timeOutHandler)

        ioInHandler = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        cpuHandler = CpuIntrInterruptionHandler(self)
        HARDWARE.interruptVector.register(CPU_INTERRUPTION_TYPE, cpuHandler)

        ioOutHandler = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)
        self._newHandler = NewInterruptionHandler(self)

        CONSOLE.configConsole()

    @property
    def flag(self):
        return  self.__flag

    @flag.setter
    def flag(self, bool):
        self.__flag = bool

    @property
    def newPid(self):
        return  self.__newPid

    @newPid.setter
    def newPid(self, number):
        self.__newPid = number

    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def pcbTable(self):
        return self._pcbTable

    """
    @property
    def readyQueue(self):
        return self._readyQueue
    """

    @property
    def readyTable(self):
        return self._readyTable

    @property
    def loader(self):
        return self._loader

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def console(self):
        return self._console

    @property
    def timeLine(self):
        return self._timeLine

    def startUP(self):
        self.install()
        while 1:
            if (self.flag == 'run'):
                self.console.executeComandLine(input('root@proyecto-SO:~/home$ '))
            else:
                break

    def install(self):
        HARDWARE.hardDisk.installProgram('/home/prg1.exe',1, [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
        HARDWARE.hardDisk.installProgram('/home/prg2.exe',2, [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
        HARDWARE.hardDisk.installProgram('/home/prg3.exe',0, [ASM.CPU(3)])

        #HARDWARE.hardDisk.installProgram('/home/prg1.exe', [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
        #HARDWARE.hardDisk.installProgram('/home/prg2.exe', [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
        #HARDWARE.hardDisk.installProgram('/home/prg3.exe', [ASM.CPU(3)])

    def updatePid(self):
        self.newPid = self.pcbTable.generatePID(self.newPid)
        return self.newPid

    def diagrama(self):
        cprint('solo hace el calculo con los procesos terminados','red')
        listaDePCB = self.pcbTable.listPCB
        BDList = list()

        for k, pcb in listaDePCB.items():  # key, pcb

            if (pcb.state == 'finished'):
                BDList.append(pcb.baseDir)

        tiempoDeEspera = BDList
        promedio = sum(tiempoDeEspera) / len(tiempoDeEspera)
        print('El promedio del tiempo de espera para este sistema dio: {}'.format(promedio))

        cprint(tabulate((self.modeGannt.print()), tablefmt="fancy_grid"),'magenta')

    def printTL(self, tickNumber):

        tl = self.timeLine.getitem(tickNumber)

        if not tl:
            cprint('isEmpty', 'green', 'on_red')
        else:
            cprint('pc          -> {}'.format(tl['pc']), 'magenta')
            cprint('ir          -> {}'.format(tl['ir']), 'magenta')
            cprint('baseDir     -> {}'.format(tl['baseDir']), 'magenta')
            cprint('limit       -> {}'.format(tl['limmit']), 'magenta')

        tl = self.timeLine.getitem(tickNumber)

        if not tl:
            cprint('isEmpty', 'green', 'on_red')
        else:
            cprint('pc          -> {}'.format(tl['pc']), 'magenta')
            cprint('ir          -> {}'.format(tl['ir']), 'magenta')
            cprint('baseDir     -> {}'.format(tl['baseDir']), 'magenta')
            cprint('limit       -> {}'.format(tl['limmit']), 'magenta')