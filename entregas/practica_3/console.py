#!/usr/bin/env python
import os
from hardware import *


class Console():
    
    def __init__(self):
        self.instructions = dict()

    def executeCLI(self, comandLine, kernel):
        #try:
        if comandLine:
            comndLineSplit = comandLine.split(' ')
        else:
            comndLineSplit = ['help']

        if len(comndLineSplit) == 1:
            self.instructions[comndLineSplit[0]](kernel)
        else:
            self.instructions[comndLineSplit[0]](kernel, comndLineSplit)
        #except:
        #       print("Oops! type help in console!")

    def configConsole(self):
            self.instructions['exit']           = self.exit
            self.instructions['load']           = self.load
            self.instructions['exe']            = self.execute
            self.instructions['run']            = self.run
            self.instructions['memory']         = self.memory
            self.instructions['kill']           = self.kill
            self.instructions['hd']             = self.hd
            self.instructions['ls']             = self.ls
            self.instructions['pause']          = self.pauseClock
            self.instructions['resume']         = self.resumeClock
            self.instructions['pc']             = self.pc
            self.instructions['ps']             = self.ps
            self.instructions['removePidPs']    = self.psRemovePid
            self.instructions['removePS']       = self.psDropAll
            self.instructions['rq']             = self.rq
            self.instructions['mmu']            = self.mmu
            self.instructions['free']           = self.free
            self.instructions['clear']          = self.clear
            self.instructions['help']           = self.help
            self.instructions['tl']             = self.timeLine
            self.instructions['gannt']          = self.gannt

    def help(self, kernel):
        print('exit         --> ShutDown OS')
        print('ls           --> Show All programs Installed')
        print('load         --> Load programs')
        print('exe          --> Load and execute programs')
        print('run          --> run load programs(PC = 0)')
        print('kill         --> kill prosses')
        print('ps           --> pcbTable')
        print('removePidPs  --> remove pid that PCBTable')
        print('removePS     --> clean PCBTable')
        print('memory       --> Display memory')
        print('free         --> display space free in memory')
        print('rq           --> show ready queue')
        print('mmu          --> show mmu')
        print('pc           --> show pc')
        print('hd           --> show hardDisk')
        print('pause        --> pause SO')
        print('resume       --> resume OS')
        print('gannt        --> calcule waiting time')
        print('tl           --> timeLine arg')
        print('clear        --> clear screen')

    def exit(self, kernel):
        HARDWARE.pc = -1
        kernel.flag = 'exit'
        HARDWARE.switchOff()

    def memory(self, kernel):
        print(HARDWARE.memory)

    def pauseClock(self, kernel):
        HARDWARE.cpu.pause()

    def resumeClock(self, kernel):
        HARDWARE.cpu.resume()

    def load(self, kernel, comandSplit):
        temp = comandSplit.pop(0)
        kernel._newHandler.execute(comandSplit)

    def execute(self, kernel, comandSplit):
        temp = comandSplit.pop(0)
        kernel._newHandler.execute(comandSplit)
        HARDWARE.cpu.pc = 0

    def run(self, kernel):
        HARDWARE.cpu.pc = 0

    def hd(self, kernel):
        HARDWARE.hardDisk.print()

    def ls(self, kernel):
        HARDWARE.hardDisk.printKey()

    def pc(self, kernel):
        print('pc: {var}'.format(var =HARDWARE.cpu.pc))

    def ps(self, kernel):
        kernel.pcbTable.print()

    def psRemovePid(self, kernel, comandSplit):
        temp = comandSplit.pop(0)
        kernel._pcbTable.removePid(comandSplit)

    def psDropAll(self, kernel):
        kernel._pcbTable.removeAll()

    def rq(self, kernel):
        #kernel._readyQueue.print()
        kernel.readyTable.print()

    def mmu(self, kernel):
        print('limit: {var}'.format(var =HARDWARE.mmu.limit))
        print('baseDir: {var}'.format(var =HARDWARE.mmu.baseDir))

    def free(self, kernel):
       print(HARDWARE.memory.free())

    def clear(self, kernel):
        os.system('clear')

    def kill(self, kernel, comandSplit):
        pid = int(comandSplit[1])
        kernel.closeProcess(pid)
        kernel._pcbTable.removePid(pid)
        print('force kill pid: {}'.format(pid))

    def timeLine(self, kernel, comandSplit):
        tickNumber = int(comandSplit[1])
        try:
            kernel.printTL(tickNumber)
        except:
            print("no se pudo cargar la imagen! tickNumber incorrecto")

    def gannt(self, kernel):
        kernel.diagrama()

CONSOLE = Console()