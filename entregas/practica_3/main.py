from hardware import *
from so import *
from log import *
import threading

class Cpu(threading.Thread):
    def run(self):
        HARDWARE.switchOn()
        print('ShutDown {}'.format(self.getName()))

class Console(threading.Thread):
    def run(self):
        startUP()
        print('ShutDown {}'.format(self.getName()))

def startUP():
        log.logger.info('Starting kernel thread')
        kernel = Kernel()
        kernel.startUP()

if __name__ == '__main__':

    #configuracion del LOG
    log.setupLogger('cpu.txt')

    ## setup our hardware and set memory size to 25 "cells"
    ## Con un Quantum de cero, siendo el quantum la segunda variable,
    ### se desactiva la interrupccion del cpu.
    HARDWARE.setup(16, 0)

    ## Threading
    cpu = Cpu(name= 'Thread-{}'.format('CPU'))
    cli = Console(name= 'Thread-{}'.format('console'))
    cli.start() 
    cpu.start()
    cpu.join()
    cli.join()
