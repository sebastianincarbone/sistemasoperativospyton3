#!/usr/bin/env python

from so import *

class NewPCB():
    def __init__(self, pid, pc, estado):
        self.__pid          =pid
        self.__pc           =pc
        self.__state        =estado

    @property
    def pid(self):
        return self.__pid

    @property
    def pc(self):
        return self.__pc

    @property
    def state(self):
        return self.__state

    @pc.setter
    def pc(self, pc):
        self.__pc = pc

    @state.setter
    def state(self, estado):
        self.__state = estado

    def modify(self, pc, state):
        self.pc         = pc
        self.state      = state

        return self

    def __repr__(self):
        return self

class ReadyQueue():
    def __init__(self):
        self._readyQueue = []

    @property
    def readyQueue(self):
        return self._readyQueue

    def addToReady(self, pid):
        self._readyQueue.append(pid)

    def getNextPid(self):
        return self._readyQueue.pop(0)

    def isEmpty(self):
        return self.readyQueue == []

    def print(self):
        print( tabulate(enumerate(self._readyQueue), ['numero','pidProgram'], tablefmt="fancy_grid"))

    def __repr__(self):
        return tabulate(enumerate(self._readyQueue), ['numero','pidProgram'], tablefmt="fancy_grid")

class Loader():

    def __init__(self, kernel):
        self._mmp = kernel.mmp
        self._memory = HARDWARE.memory
        self._hardDisk = HARDWARE.hardDisk
        self._mmu = HARDWARE.mmu
        self.__baseDir = 0

    @property
    def mmp(self):
        return self._mmp

    @property
    def mmu(self):
        return self._mmu

    @property
    def memory(self):
        return self._memory

    @property
    def hardDisk(self):
        return self._hardDisk

    @property
    def baseDir(self):
        return self.__baseDir

    @baseDir.setter
    def baseDir(self, baseDirnew):
        self.__baseDir = baseDirnew

    def load_page(self,page,frame):
        index = (frame * self.mmp.frameSize)
        for instruccion in page:
            HARDWARE.memory.put(index, instruccion)
            index += 1

    def loadProgram (self, progName, newPid):
        programInFrames = self.hardDisk.getProgram(progName)
        cantidadDePaginas = len(programInFrames)

        self.mmp.setFrames(newPid,cantidadDePaginas)

        for pg in range(0,cantidadDePaginas):
            page = programInFrames[pg]
            self.load_page(page,self.mmp.searchFrame(newPid,pg))

        pcb = NewPCB(newPid, -1, 'New')

        return pcb

##------- INTERRUPTIONS

#-- IO

class IoInInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):

        pcbRun = self.kernel.pcbTable.returnRunningPCB()

        self.kernel.pcbTable.updatePCofPcb(pcbRun, HARDWARE.cpu.pc)
        self.kernel.pcbTable.changeState(pcbRun, 'waiting')
        self.kernel.dispatcher.save(pcbRun)

        operation = irq.parameters
        self.kernel.ioDeviceController.runOperation(pcbRun, operation)

        if not self.kernel.readyQueue.isEmpty():

            pidNextFromRQ = self.kernel.readyQueue.getNextPid()
            pcbToLoad = self.kernel.pcbTable.findPcb(pidNextFromRQ)

            self.kernel.pcbTable.changeState(pcbToLoad, 'running')
            self.kernel.dispatcher.load(pcbToLoad)

class IoOutInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):

        pcbFromIO = self.kernel.ioDeviceController.getFinishedPCB()

        if self.kernel.pcbTable.isAnyRunning():
            self.kernel.pcbTable.changeState(pcbFromIO, 'ready')
            self.kernel.readyQueue.addToReady(pcbFromIO.pid)

        else:
            self.kernel._pcbTable.changeState(pcbFromIO, 'running')
            self.kernel._dispatcher.load(pcbFromIO)

#-- NEW / KILL

class NewInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, pathlist):

        for path in pathlist:

            programName = '/home/'+path+'.exe'
            pcbToLoad =  self.kernel.loader.loadProgram(programName, self.kernel.updatePid())

            #Carga en la pcbTable
            self.kernel.pcbTable.addPcbToTable(pcbToLoad)

            if self.kernel.pcbTable.isAnyRunning():
                self.kernel.pcbTable.changeState(pcbToLoad, 'ready')
                self.kernel.readyQueue.addToReady(pcbToLoad.pid)
                print('Load to readyQueue')

            else:
                self.kernel.pcbTable.changeState(pcbToLoad, 'running')
                self.kernel.dispatcher.load(pcbToLoad)
                print('running procces')


class KillInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):

        pcbToKill = self.kernel.pcbTable.returnRunningPCB()

        self.kernel.pcbTable.changeState(pcbToKill, 'finished')

        if self.kernel.readyQueue.isEmpty():

            HARDWARE.cpu.pc = -1

        else:

            pidToLoad = self.kernel.readyQueue.getNextPid()
            pcbToLoad = self.kernel.pcbTable.findPcb(pidToLoad)

            self.kernel.pcbTable.changeState(pcbToLoad, 'running')
            self.kernel.dispatcher.load(pcbToLoad)
