#!/usr/bin/env python
from re import escape

from hardware import *
from console import  *
from interrupcciones import *
from SchedulerRR import *
##Schedulers
    #SchedulerFCFS
    #SchedulerRR
    #SchedulerExpropiativo
    #SchedulerNoExpropiativo
    #SchedullerExpropiativoConRR
from tabulate import tabulate
from termcolor import cprint

class NewPCBTable():

    # El PCBTable tipo {pid, pcb}
    # En esta clase definimos los programas que estan cargados en memoria y le establecemos un estado
    # Metodos establecidos son:
    #   extractFromTable(self, pcb) -> saca pcb del diccionario
    #   isRunning(self, Pid) -> denota el pcb que se esta corriendo mediante un runningPid
    #   addPCBTable(self, pcb) -> agrega un pcb al diccionario
    #   generatePID(self, pcb) -> genera un PID como primaryKey para cada PCB del diccionario
    #   modificarPCBEnTable (pcb, baseDir,limmit,pcActual,estado) -> modifica un elemento de la lista

    def __init__(self):
        self.__listPCB = dict()
        self.__pid = 0
        self.__runningPid = -1

    @property
    def listPCB(self):
        return self.__listPCB

    @property
    def runningPid(self):
        return self.__runningPid

    @runningPid.setter
    def runningPid(self, runningPidNew):
        self.__runningPid = runningPidNew

    @property
    def pid(self):
        return self.__pid

    @pid.setter
    def pid(self, pidNew):
        self.__pid = pidNew

    def isAnyRunning(self):
        return self.runningPid != -1

    def returnRunningPCB(self):
        return self.listPCB[self.runningPid]

    def findPcb(self, pid):
        return self.listPCB[pid]

    def addPcbToTable(self, pcb):
        self.listPCB[pcb.pid] = pcb

    def generatePID(self, pid):
        return pid + 1

    def modifyPcb(self, pcb, pcActual, state):
        self.listPCB[pcb.pid] = pcb.modify(pcActual, state)

    def updatePCofPcb(self, pcb, pc):
        return pcb.modify(pc, pcb.state)

    def changeState(self, pcb, state):

        if state == 'running':
            self.runningPid = pcb.pid
        elif state == 'waiting':
            self.runningPid = -1
        elif state == 'finished':
            self.runningPid = -1

        return pcb.modify(pcb.pc, state)

    def toUpgrade(self):
        retorno = list()
        for k, pcb in self.__listPCB.items():
            if pcb.age >= 3:
                pcb.age = 0
                retorno.append((pcb.age,pcb.lvl))
            else:
                pcb.age = pcb.age + 1

        return retorno

    def print(self):
        retorno = self.listPCB
        if not retorno:
            print('isEmpty')
        else:
            headers = ['Pid', 'pc', 'state']

            data = sorted(
                [(p.pid, p.pc, p.state) for k, p in retorno.items()])

            print(tabulate(data, headers=headers, tablefmt="fancy_grid"))

    def modeGannt(self):
        listaLocal = self.listPCB
        listaProgramaEstado = list()
        if listaLocal:
            for key, pcb in listaLocal.items():
                if   pcb.state == 'ready':
                    prog = key
                    listaProgramaEstado.append('Ready')
                elif pcb.state == 'running':
                    prog = key
                    listaProgramaEstado.append('Run')
                elif pcb.state == 'waiting':
                    prog = key
                    listaProgramaEstado.append('Blocked')
                elif pcb.state == 'finished':
                    prog = key
                    listaProgramaEstado.append('Finished')
            return  listaProgramaEstado
        else:
            return  'nill'

    def __repr__(self):
        retorno = self.listPCB
        if not retorno:
            print('isEmpty')
        else:
            headers = ['Pid', 'pcActual', 'baseDir', 'limit', 'estado']

            data = sorted(
                [(p.pid, p.pc, p.baseDir, p.limmit, p.estado) for k, p in retorno.items()])

            print(tabulate(data, headers=headers, tablefmt="fancy_grid"))

class MemoryManager():
    def __init__(self, frameSize):
        self.__frameSize = frameSize
        self.__freeFrame = list()
        cantidadDeFramesDisponibles = int(HARDWARE.memory.size / frameSize)
        for i in range(0, cantidadDeFramesDisponibles):
            self.__freeFrame.append(i)
        self.__pageTable = PageTable()

    @property
    def freeFrame(self):
        return self.__freeFrame

    @property
    def pageTable(self):
        return self.__pageTable

    @property
    def frameSize(self):
        return self.__frameSize

    def setFrames(self,pid,cantidadDePaginasNecesarias):
        if cantidadDePaginasNecesarias <= len(self.freeFrame):
            for i in range(0,cantidadDePaginasNecesarias):
                self.addRow(pid,i,self.freeFrame.pop(0))
        else:
            return  0

    def searchListFrameByPid(self,pid):
        return self.pageTable.searchByPid(pid)

    def searchFrame(self,pid,pageForSearch):
        return self.pageTable.searchFrame(pid, pageForSearch)

    def addRow(self,pid,page,frame):
        self.pageTable.addRow(pid,page,frame)

class PageTable():
    def __init__(self):
        self.__pageTable = dict()

    @property
    def pageTable(self):
        return self.__pageTable

    def addRow(self, pid, page, frame):
        par = (page, frame)
        try:
            self.pageTable[pid].append(par)
        except:

            self.pageTable[pid] = [par]

    def searchFrame(self,pid,pageForSearch):
        framesTable =  self.pageTable[pid]

        for page,frame in framesTable:
            if page == pageForSearch:
                return frame

    def searchByPid(self,pid):
        return self.pageTable[pid]

    def rmRow(self, pid):
        del pageTable[pid]

class TimeLine():
    def __init__(self, kernel):
        self.__timeLine = dict()
        self.__tickNumber = 0
        self.__kernel = kernel
        self.__hardware = HARDWARE

    @property
    def timeLine(self):
        return self.__timeLine

    @property
    def tickNumber(self):
        return self.__tickNumber

    @tickNumber.setter
    def tickNumber(self, tickNumber):
        self.__tickNumber = tickNumber

    @property
    def kernel(self):
        return self.__kernel

    @property
    def hardware(self):
        return self.__hardware

    def save(self):
        self.timeLine[self.tickNumber] = Imagen(self.kernel).generate()
        self.tickNumber += 1

    def remove(self, tickNumber):
        self.timeLine.__delitem__(tickNumber)

    def getitem(self, item):
        return self.timeLine[item]

class Imagen():
    def __init__(self, kernel):
        self.__imgLocal = dict()
        self.__hardware = HARDWARE

    @property
    def imgLocal(self):
        return self.__imgLocal

    @property
    def hw(self):
        return self.__hardware

    def generate(self):
        self.imgLocal['pc'] = self.hw.cpu.pc
        self.imgLocal['ir'] = self.hw.cpu._ir
        return self.__imgLocal

class Dispatcher ():
    def __init__(self,kernel):
        self.__cpu = HARDWARE.cpu
        self.__mmu = HARDWARE.mmu
        self.__kernel = kernel

    @property
    def cpu(self):
        return self.__cpu

    @property
    def mmu(self):
        return self.__mmu

    @property
    def kernel(self):
        return self.__kernel

    def load (self,pcb):
        if pcb.pc == -1:
            self.cpu.pc = 0
        else :
            self.cpu.pc = pcb.pc

        self.mmu.receive(self.kernel._mmp.searchListFrameByPid(pcb.pid))

    def save (self, pcb):
        self.kernel.pcbTable.modifyPcb(pcb, self.cpu.pc, pcb.state)
        self.cpu.pc = -1

class ModeGannt():
    def __init__(self):
        self.__tupleList = list()

    @property
    def tupleList(self):
        return self.__tupleList

    def save(self,listaDeProgramaEstado):
        if listaDeProgramaEstado != 'nill':
            self.tupleList.append(listaDeProgramaEstado)

    def print(self):
        return  self.tupleList

class Console():
    def __init__(self, kernel, console):
        self.__kernel = kernel
        self.console = console
        console.configConsole()

    @property
    def kernel(self):
        return self.__kernel

    def executeComandLine(self, comand):
        self.console.executeCLI(comand, self.kernel)

class Kernel():

    def __init__(self):

        self.__newPid = 0
        self.__flag = 'run'

        self._readyQueue    = ReadyQueue()
        self._dispatcher    = Dispatcher(self)
        self.modeGannt      = ModeGannt()
        self._timeLine      = TimeLine(self)
        self._pcbTable      = NewPCBTable()
        self._console       = Console(self, CONSOLE)
        self._mmp           = MemoryManager(4) #establecido con un frame size de 4
        self._loader        = Loader(self)

        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        timeOutHandler = TimeOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIME_OUT_INTERRUPTION_TYPE, timeOutHandler)

        ioInHandler = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        cpuHandler = CpuIntrInterruptionHandler(self)
        HARDWARE.interruptVector.register(CPU_INTERRUPTION_TYPE, cpuHandler)

        ioOutHandler = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)
        self._newHandler = NewInterruptionHandler(self)

    @property
    def mmp(self):
        return  self._mmp

    @property
    def flag(self):
        return  self.__flag

    @flag.setter
    def flag(self, bool):
        self.__flag = bool

    @property
    def newPid(self):
        return  self.__newPid

    @newPid.setter
    def newPid(self, number):
        self.__newPid = number

    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def pcbTable(self):
        return self._pcbTable

    """
    @property
    def readyQueue(self):
        return self._readyQueue
    """

    @property
    def readyQueue(self):
        return self._readyQueue

    @property
    def loader(self):
        return self._loader

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def console(self):
        return self._console

    @property
    def timeLine(self):
        return self._timeLine

    def startUP(self):
        self.install()
        while 1:
            if (self.flag == 'run'):
                self.console.executeComandLine(input('root@proyecto-SO:~/home$ '))
            else:
                break

    def install(self):
        #HARDWARE.hardDisk.installProgram('/home/prg1.exe',1, [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
        #HARDWARE.hardDisk.installProgram('/home/prg2.exe',2, [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
        #HARDWARE.hardDisk.installProgram('/home/prg3.exe',0, [ASM.CPU(3)])

        HARDWARE.hardDisk.installProgram('/home/prg1.exe', [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
        HARDWARE.hardDisk.installProgram('/home/prg2.exe', [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
        HARDWARE.hardDisk.installProgram('/home/prg3.exe', [ASM.CPU(3)])

    def updatePid(self):
        self.newPid = self.pcbTable.generatePID(self.newPid)
        return self.newPid

    def diagrama(self):
        cprint('solo hace el calculo con los procesos terminados','red')
        listaDePCB = self.pcbTable.listPCB
        BDList = list()

        for k, pcb in listaDePCB.items():  # key, pcb

            if (pcb.state == 'finished'):
                BDList.append(pcb.baseDir)

        tiempoDeEspera = BDList
        promedio = sum(tiempoDeEspera) / len(tiempoDeEspera)
        print('El promedio del tiempo de espera para este sistema dio: {}'.format(promedio))

        cprint(tabulate((self.modeGannt.print()), tablefmt="fancy_grid"),'magenta')

    def printTL(self, tickNumber):

        tl = self.timeLine.getitem(tickNumber)

        if not tl:
            cprint('isEmpty', 'green', 'on_red')
        else:
            cprint('pc          -> {}'.format(tl['pc']), 'magenta')
            cprint('ir          -> {}'.format(tl['ir']), 'magenta')
            cprint('baseDir     -> {}'.format(tl['baseDir']), 'magenta')
            cprint('limit       -> {}'.format(tl['limmit']), 'magenta')

        tl = self.timeLine.getitem(tickNumber)

        if not tl:
            cprint('isEmpty', 'green', 'on_red')
        else:
            cprint('pc          -> {}'.format(tl['pc']), 'magenta')
            cprint('ir          -> {}'.format(tl['ir']), 'magenta')
            cprint('baseDir     -> {}'.format(tl['baseDir']), 'magenta')
            cprint('limit       -> {}'.format(tl['limmit']), 'magenta')
