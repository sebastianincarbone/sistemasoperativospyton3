from hardware import *
from so import *
from log import *
import threading
import interfaze as interface

class Cpu(threading.Thread):
    def run(self):
        HARDWARE.switchOn()
        print('ShutDown {}'.format(self.getName()))

class Console(threading.Thread):
    def run(self):
        startUP()
        print('ShutDown {}'.format(self.getName()))

def startUP():
    log.logger.info('Starting kernel thread')
    Kernel().startUP()


if __name__ == '__main__':

    #configuracion del LOG
    log.setupLogger('cpu.txt')

    ##setup del hardware estableciendo 16 celdas de memoia.
    ## siendo el quantum la segunda variable, se desactiva la interrupccion del cpu con un valor de 0.
    HARDWARE.setup(16, 2)

    ## Threading
    cpu = Cpu(name= 'Thread-{}'.format('CPU'))
    cli = Console(name='Thread-{}'.format('console'))

    cli.start()
    cpu.start()
    cpu.join()
    cli.join()