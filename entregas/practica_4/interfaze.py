from tkinter import *
from hardware import *
import threading,time

'''
Importante!
puedo importar la consola para poder empezar a tirar comandos ahi, como el exit cuando preciono el boton!
'''


#---------------------
def start():
    print("started")
def pause():
    print("pause")
def resume():
    print("resume")
def exit1():
    print('exit(0)')
#---------------------

class initInterface(threading.Thread):
    def run(self):
        startUP()
        print('ShutDown {}'.format(self.getName()))

class Interface():
    def __init__(self):
        self.__tk = Tk()
        self.__tk.title("sistemasOperativos")
        self.__pc = StringVar()
        self.__ir = StringVar()
        self.__mmuBaseDir = StringVar()
        self.__mmuLimit = StringVar()

        self.conrrer(self)

    @property
    def tk(self):
        return self.__tk

    @property
    def pc(self):
        return self.__pc

    @property
    def ir(self):
        return self.__ir

    @property
    def mmuBaseDir(self):
        return self.__mmuBaseDir

    @property
    def mmuLimit(self):
        return self.__mmuLimit

    def upData(self):
        lista = HARDWARE.upDate()
        self.__pc.set(lista[0])
        self.__ir.set(lista[1])

    def text(self,frame ,text ,x ,y ,color ,size ,padx ,pady):
        level = Label(frame,text=text, textvariable=text, fg=color, font=size)
        level.grid(row=x, column=y, padx=padx, pady=pady)

    def input(self ,frame ,x ,y ,padx ,pady):
        entry = Entry(frame)
        entry.grid(row=x, column=y, padx=padx, pady=pady)

    def button(self, text, function):
        boton = Button(self.tk, text=text,command=function)
        boton.pack(side=LEFT,padx=10,pady=20)

    def clear(self):
        self.__frame.update()

    def close(self):
        self.tk.destroy()

    def conrrer(self, inter):
        self.__frame = Frame(self.__tk,height=500,width=500)
        self.__frame.pack()
        # --------------------------------------------------------------------
        # inter.text(frame, text, 0, 0,'red', 10, 50, 10)
        # inter.input(frame, 0, 1, 10, 10)
        # ----------------------------------------------------------------CPU
        inter.text(self.__frame, '--------CPU--------', 0, 1, 'red', 10, 50, 10)

        inter.text(self.__frame, 'pc :', 1, 0, 'black', 10, 20, 10)
        inter.text(self.__frame, inter.pc, 1, 1, 'black', 10, 20, 10)

        inter.text(self.__frame, 'ir :', 1, 2, 'red', 10, 20, 10)
        inter.text(self.__frame, inter.ir, 1, 3, 'red', 10, 20, 10)

        '''# ----------------------------------------------------------------MMU
        inter.text(self.__frame, '--------MMU--------', 2, 1, 'red', 10, 50, 10)

        inter.text(self.__frame, 'baseDir :', 3, 0, 'red', 10, 20, 10)
        inter.text(self.__frame, inter.mmuBaseDir, 3, 1, 'red', 10, 20, 10)

        inter.text(self.__frame, 'limit :', 3, 2, 'red', 10, 20, 10)
        inter.text(self.__frame, inter.mmuLimit, 3, 3, 'red', 10, 20, 10)
        '''
        #inter.button('resume', resume)
        #inter.button('pause', pause)
        #inter.button('start', start)
        inter.button(text='Exit', function=self.close)

def startUP():
    inter = Interface()
    while True:
        inter.tk.update_idletasks()
        inter.tk.update()
        inter.upData()
        time.sleep(0.1)