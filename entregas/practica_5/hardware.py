#!/usr/bin/env python
from   tabulate import tabulate
from   time     import sleep
import log



##  Estas son la instrucciones soportadas por nuestro CPU
INSTRUCTION_IO = 'IO'
INSTRUCTION_CPU = 'CPU'
INSTRUCTION_EXIT = 'EXIT'

##  Estas son la interrupciones soportadas por nuestro Kernel
KILL_INTERRUPTION_TYPE = "#KILL"
NEW_INTERRUPTION_TYPE = "#NEW"
IO_IN_INTERRUPTION_TYPE = "#IO_IN"
IO_OUT_INTERRUPTION_TYPE = "#IO_OUT"
CPU_INTERRUPTION_TYPE = "#CPU"
TIME_OUT_INTERRUPTION_TYPE = "#TIME_OUT"
PAGE_FAULT_INTERRUPTION_TYPE = "#PAGE_FAULT"

class ASM():

    @classmethod
    def EXIT(self, times):
        return [INSTRUCTION_EXIT] * times

    @classmethod
    def IO(self):
        return INSTRUCTION_IO

    @classmethod
    def CPU(self, times):
        return [INSTRUCTION_CPU] * times

    @classmethod
    def isEXIT(self, instruction):
        return INSTRUCTION_EXIT == instruction

    @classmethod
    def isIO(self, instruction):
        return INSTRUCTION_IO == instruction

class IRQ:

    def __init__(self, type, parameters = None):
        self._type = type
        self._parameters = parameters

    @property
    def parameters(self):
        return self._parameters

    @property
    def type(self):
        return self._type

class InterruptVector():

    def __init__(self):
        self._handlers = dict()

    def register(self, interruptionType, interruptionHandler):
        self._handlers[interruptionType] = interruptionHandler

    def handle(self, irq):
        log.logger.info("Handling {type} irq with parameters = {parameters}".format(type=irq.type, parameters=irq.parameters ))
        self._handlers[irq.type].execute(irq)

class Clock():

    def __init__(self):
        self._subscribers = []
        self._running = False

    def addSubscriber(self, subscriber):
        self._subscribers.append(subscriber)

    def stop(self):
        self._running = False

    def start(self):
        log.logger.info("---- :::: START CLOCK  ::: -----")
        self._running = True
        tickNbr = 0
        while (self._running):
            self.tick(tickNbr)
            tickNbr += 1

    def tick(self, tickNbr):
        log.logger.info("------------------------------[ tick: {tickNbr} ]------------------------------".format(tickNbr = tickNbr))
        ## notify all subscriber that a new clock cycle has started
        for subscriber in self._subscribers:
            subscriber.tick(tickNbr)
        ## wait 1 second and keep looping
        sleep(1)
        
    def do_ticks(self, times):
        log.logger.info("---- :::: CLOCK do_ticks: {times} ::: -----".format(times=times))
        for tickNbr in range(0, times):
            self.tick(tickNbr)

class Memory():

    def __init__(self, size, frameSize):
        self.__size = size
        self._cells = [''] * size
        self._frameSize = frameSize

    @property
    def frameSize(self):
        return self._frameSize

    @property
    def size(self):
        return self.__size

    def put(self, addr, value):
        self._cells[addr] = value

    def get(self, addr):
        return self._cells[addr]

    def free(self):
        varReturn = 0
        for cell in self._cells:
            if not cell: varReturn += 1
        return varReturn

    def duplicar (self):
        return self._cells[:]

    def __repr__(self):
        return tabulate(enumerate(self._cells), tablefmt='psql')

class MMU():

    def __init__(self, memory):
        self._memory = memory
        self._pageTable = list()

    @property
    def pageTable(self):
        return self._pageTable

    def __dropPageTable(self):
        self._pageTable = list()

    def deletePageTable(self):
        self.__dropPageTable()

    def __add(self,page,frame=-1,tick=-1):
        newRow = {  'page'  : page,
                    'frame' : frame,
                    'tick'  : tick  }
        self.pageTable.append(newRow)

    def receive (self, page):
        self.__add(page)
        log.logger.info("---- ::::MMU UPDATE::: -----")
        log.logger.info(tabulate(self.pageTable))

    def setFrame (self, page, frame):
        for r in self._pageTable:
            if r['page'] == page:
                r['frame'] = frame

    def searchInTable(self,searchedPage):
        for r in self.pageTable:
            if r['page'] == searchedPage:
                return r['frame']
        return -1

    def fetch(self,  pc, interruptionVector):
        frameSize = self._memory.frameSize
        page = pc // frameSize
        offset = pc % frameSize

        frame = self.searchInTable(page)
        if frame == -1:
            pageFaultIRQ = IRQ(PAGE_FAULT_INTERRUPTION_TYPE, page)
            interruptionVector.handle(pageFaultIRQ)
            frame = self.searchInTable(page)

        return self._memory.get(frame * frameSize + offset)

class Cpu():

    def __init__(self, mmu, interruptVector, quantum):
        self._mmu = mmu
        self._interruptVector = interruptVector
        self._pc = -1
        self._ir = None
        self._countTicks = 0
        self._quantum = quantum
        self.pauseFlag = 'end'

    def tick(self, tickNbr):
        if self.pauseFlag == 'pause':
            log.logger.info("---- :::: PAUSE ::: -----")
            while 1:

                sleep(.5)
                if self.pauseFlag == 'end':
                    log.logger.info("---- :::: RESUME ::: -----")
                    break

        self._countTicks += 1

        cpuIRQ = IRQ(CPU_INTERRUPTION_TYPE, self._ir)
        self._interruptVector.handle(cpuIRQ)

        if (self._quantum != 0 and self._countTicks > self._quantum):
            self._countTicks = 1
            timeOutIRQ = IRQ(TIME_OUT_INTERRUPTION_TYPE, self._ir)
            self._interruptVector.handle(timeOutIRQ)
        
        if (self._pc > -1):
            self._fetch()
            self._decode()
            self._execute()

        else:
            log.logger.info("cpu - NOOP")

    def resetCountTicks(self):
        self._countTicks = 1

    def pause(self):
        self.pauseFlag = 'pause'

    def resume(self):
        self.pauseFlag = 'end'

    def _fetch(self):
        self._ir = self._mmu.fetch(self._pc, self._interruptVector)
        self._pc += 1

    def _decode(self):
        pass

    def _execute(self):
        if ASM.isEXIT(self._ir):
            killIRQ = IRQ(KILL_INTERRUPTION_TYPE)
            self._interruptVector.handle(killIRQ)
        elif ASM.isIO(self._ir):
            ioInIRQ = IRQ(IO_IN_INTERRUPTION_TYPE, self._ir)
            self._interruptVector.handle(ioInIRQ)
        else:
            log.logger.info("cpu - Exec: {instr}, PC={pc}".format(instr=self._ir, pc=self._pc))

    @property
    def pc(self):
        return self._pc

    def setPC(self, newPc):
        self._pc = newPc

    @property
    def quantum(self):
        return self._quantum

    @quantum.setter
    def quantum(self, quantumNew):
        self._quantum = quantumNew

    def __repr__(self):
        return "CPU(PC={pc})".format(pc=self._pc)

class AbstractIODevice():

    def __init__(self, deviceId, deviceTime):
        self._deviceId = deviceId
        self._deviceTime = deviceTime
        self._busy = False

    @property
    def deviceId(self):
        return self._deviceId

    @property
    def is_busy(self):
        return self._busy

    @property
    def is_idle(self):
        return not self._busy


    ## executes an I/O instruction
    def execute(self, operation):
        if (self._busy):
            raise Exception("Device {id} is busy, can't  execute operation: {op}".format(id = self.deviceId, op = operation))
        else:
            self._busy = True
            self._ticksCount = 0
            self._operation = operation

    def tick(self, tickNbr):
        if (self._busy):
            self._ticksCount += 1
            if (self._ticksCount > self._deviceTime):
                ## operation execution has finished
                self._busy = False
                ioOutIRQ = IRQ(IO_OUT_INTERRUPTION_TYPE, self._deviceId)
                HARDWARE.interruptVector.handle(ioOutIRQ)
            else:
                log.logger.info("device {deviceId} - Busy: {ticksCount} of {deviceTime}".format(deviceId = self.deviceId, ticksCount = self._ticksCount, deviceTime = self._deviceTime))

class PrinterIODevice(AbstractIODevice):
    def __init__(self):
        super(PrinterIODevice, self).__init__("Printer", 3)

class HardDisk:
    def __init__(self):
        self.__memory        = dict()
        self.__memorySWAP    = dict()
        self.__freeFrameSwap = 0

    @property
    def freeFrameSwap(self):
        return self.__freeFrameSwap

    def installProgram(self, path, listInstruccions):
        listaDeIntruccionesCompleta = self.code(listInstruccions)
        self.__memory[path] = self.paginador(listaDeIntruccionesCompleta)

    def paginador(self, listaDeInstrucciones):
        retorno = list()
        frameSize = 4
        listaDeInst = listaDeInstrucciones
        listaDeInst_len = len(listaDeInst)

        while listaDeInst_len > 0:
            retorno.append(listaDeInst[0:frameSize])

            for i in range(0,frameSize):
                if listaDeInst:
                    listaDeInst.pop(0)

            listaDeInst_len = listaDeInst_len - frameSize

        return  retorno

    def getProgram(self, path):
        return self.__memory[path]

    def code(self, listInstruccions):
        expanded = []
        for i in listInstruccions:
            if isinstance(i, list):
                expanded.extend(i)
            else:
                expanded.append(i)
        last = expanded[-1]
        if not ASM.isEXIT(last):
            expanded.append(INSTRUCTION_EXIT)
        return expanded

    def swapIn(self, address, listOfInstructions):
        log.logger.info("--------Swapping in : {}".format(listOfInstructions))
        self.__memorySWAP[address] = listOfInstructions

    def swapOut(self, address):
        try:
            return self.__memorySWAP[address]
        except:
            log.logger.info("proceso no encontrado")

    def getFreeSwapFrame(self):
        retorno = self.freeFrameSwap
        self.__freeFrameSwap +=1
        return retorno

    def print(self):
        header = self.__memory.keys()
        log.logger.info(tabulate(self.__memory,header, tablefmt="fancy_grid"))

    def printKey(self):
        keys = self.__memory.keys()
        for element in keys:
            print(element.split('/')[2])

class Hardware():

    ## Setup our hardware
    def setup(self, memorySize, frameSize, quantum):
        ## add the components to the "motherboard"
        self._memory = Memory(memorySize, frameSize)
        self._interruptVector = InterruptVector()
        self._clock = Clock()
        self._ioDevice = PrinterIODevice()
        self._mmu = MMU(self._memory)
        self._cpu = Cpu(self._mmu, self._interruptVector, quantum)
        self._clock.addSubscriber(self._ioDevice)
        self._clock.addSubscriber(self._cpu)
        self._hardDisk = HardDisk()

    def switchOn(self):
        log.logger.info(" ---- SWITCH ON ---- ")
        return self.clock.start()

    def switchOff(self):
        self.clock.stop()
        log.logger.info(" ---- SWITCH OFF ---- ")

    @property
    def cpu(self):
        return self._cpu

    @property
    def clock(self):
        return self._clock

    @property
    def interruptVector(self):
        return self._interruptVector

    @property
    def memory(self):
        return self._memory

    @property
    def mmu(self):
        return self._mmu

    @property
    def ioDevice(self):
        return self._ioDevice

    @property
    def hardDisk(self):
        return self._hardDisk

    def upDate(self):
        return [HARDWARE.cpu.pc, HARDWARE.cpu._ir]

    def __repr__(self):
        return "HARDWARE state {cpu}\n{mem}".format(cpu=self._cpu, mem=self._memory)

HARDWARE = Hardware()
