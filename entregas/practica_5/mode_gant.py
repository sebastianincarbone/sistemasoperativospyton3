from  tabulate  import  tabulate
import copy
import termcolor
import os

class Historico():
    def __init__(self):
        self.__timeLine         = list()
        self.__modeGantTable    = list()
        self.__ticks            = ['pid \ tick']
        self.__index            = 0
        self.__flag             = False
        self.__totalWaitingTime = 0
        self.__tiempoDeRetorno  = 0

    def __construirLista(self,pid):
        listaRetorno = list()
        listaRetorno.append(pid)
        if self.__index == 0:
            return listaRetorno
        else:
            for index in range(0, self.__index):
                listaRetorno.append(termcolor.colored('*','cyan'))
            return listaRetorno

    def __hayQueActualizarlo(self,pid):
        for row in self.__modeGantTable:
            if row.count(pid) > 0 :
                return True
        return False

    def __actualizarTabla(self,pid,state):
        color = {'waiting':'magenta','finished':'red','new':'cyan','ready':'blue','running':'green'}
        letra = {'waiting':'W','finished':'F','new':'N','ready':'R','running':'RUN'}
        self.__tiempoDeRetorno += 1
        if letra[state] == 'R' or letra[state] == 'N':
            self.__totalWaitingTime += 1

        state = termcolor.colored(letra[state],color[state])
        for row in self.__modeGantTable:
            if row.count(pid) > 0:
                row.append(state)

    def __save(self,dictOfPCB):
        self.__ticks.append(self.__index)
        self.__index += 1
        listOfKeys = dictOfPCB.keys()
        for pid in listOfKeys:
            if self.__hayQueActualizarlo(pid):
               self.__actualizarTabla(pid,dictOfPCB[pid].state)
            else:
                self.__modeGantTable.append(self.__construirLista(dictOfPCB[pid].pid))

    def startRecord(self, bool):
        self.__flag = bool

    def saveImg(self,memory, listPCB):
        if self.__flag:
            self.__save(listPCB)
            self.__timeLine.append([memory.duplicar(), copy.deepcopy(listPCB)])

    def modeGant(self):
        termcolor.cprint("------------------------------------  : MODEGANTT :  ------------------------------------",'red')
        #with open('file','w') as f:
        #    f.write(tabulate(self.__modeGantTable, self.__ticks, tablefmt="fancy_grid"))
        os.system('/usr/bin/less -S {}'.format(tabulate(self.__modeGantTable, self.__ticks, tablefmt="fancy_grid")))
        #print(tabulate(self.__modeGantTable, self.__ticks, tablefmt="fancy_grid"))

        termcolor.cprint("------------------------------------  : PROMEDIOS :  ------------------------------------",'red')
        print("**tiempoTotalDeEspera : {}".format(self.__totalWaitingTime))
        print("****")
        print("**TiempoDeRetorno     : {}".format(self.__tiempoDeRetorno))
        termcolor.cprint("------------------------------------  ::::::::::  ------------------------------------",'red')

    def timeLine(self, i):
        termcolor.cprint("------------------------------------  :TIMELINE:  ------------------------------------",'red')
        print("**TickNumber : {}".format(i))
        print("**Memory")
        print(self.__timeLine[i][0])
        print("**PCBTable")
        retorno = self.__timeLine[i][1]
        if not retorno:
            print('isEmpty')
        else:
            headers = ['Pid', 'Name', 'lvl', 'pageToLoad', 'pc', 'state']

            data = sorted([(p.pid, p.nombre, p.lvl, p.pageToLoad, p.pc, p.state) for k, p in retorno.items()])

            print(tabulate(data, headers=headers, tablefmt="fancy_grid"))


GANTMODE = Historico()