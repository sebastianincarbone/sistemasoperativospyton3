#!/usr/bin/env python
import os
import termcolor
from   hardware import *
from mode_gant import GANTMODE


class Console():

    def __init__(self):
        self.instructions = dict()

    def executeCLI(self, comandLine, kernel):

        try:
            if comandLine:
                comndLineSplit = comandLine.split(' ')
            else:
                comndLineSplit = ['help']

            if len(comndLineSplit) == 1:
                self.instructions[comndLineSplit[0]](kernel)
            else:
                self.instructions[comndLineSplit[0]](kernel, comndLineSplit)
        except:
            print("Oops! type help in console!")

    def configConsole(self):
            self.instructions['exit']           = self.exit
            self.instructions['load']           = self.load
            self.instructions['memory']         = self.memory
            self.instructions['kill']           = self.kill
            self.instructions['hd']             = self.hd
            self.instructions['ls']             = self.ls
            self.instructions['pause']          = self.pauseClock
            self.instructions['resume']         = self.resumeClock
            self.instructions['pc']             = self.pc
            self.instructions['ps']             = self.ps
            self.instructions['rt']             = self.readyTable
            self.instructions['free']           = self.free
            self.instructions['clear']          = self.clear
            self.instructions['help']           = self.help
            self.instructions['gantt']           = self.gannt
            self.instructions['tl']             = self.timeLine
            self.instructions['nano']             = self.nano
            self.instructions['info']           = self.info

    def help(self, kernel):
        print('exit         --> ShutDown OS')
        print('ls           --> Show All programs Installed')
        print('load         --> Load programs')
        print('kill         --> kill prosses')
        print('ps           --> Show pcbTable')
        print('memory       --> Display memory')
        print('free         --> display free space in memory')
        print('rt           --> Show ready table')
        print('tl           --> Show pcbTable & memory at certain tick')
        print('nano         --> Write your own programs!')
        print('help         --> Shows this help')
        print('info         --> Show system Info')
        print('pc           --> Show pc')
        print('hd           --> Show hardDisk')
        print('pause        --> Pause SO')
        print('resume       --> Resume OS')
        print('gantt        --> Calcule waiting time')
        print('clear        --> Clear screen')

    def exit(self, kernel):
        HARDWARE.pc = -1
        kernel.flag = 'exit'
        HARDWARE.switchOff()

    def memory(self, kernel):
        print(HARDWARE.memory)

    def pauseClock(self, kernel):
        HARDWARE.cpu.pause()

    def resumeClock(self, kernel):
        HARDWARE.cpu.resume()

    def load(self, kernel, comandSplit):
        temp = comandSplit.pop(0)
        kernel.loadProgramList(comandSplit)
        while kernel.programList:
            kernel._newHandler.execute(kernel.getNextProgram())

    def hd(self, kernel):
        HARDWARE.hardDisk.print()

    def ls(self, kernel):
        HARDWARE.hardDisk.printKey()

    def pc(self, kernel):
        print('pc: {var}'.format(var =HARDWARE.cpu.pc))

    def ps(self, kernel):
        print(kernel.pcbTable)

    def psRemovePid(self, kernel, comandSplit):
        temp = comandSplit.pop(0)
        kernel._pcbTable.removePid(comandSplit)

    def psDropAll(self, kernel):
        kernel._pcbTable.removeAll()

    def readyTable(self, kernel):
        print(kernel.scheduler.readyTable)

    def mmu(self, kernel):
        print('limit: {var}'.format(var =HARDWARE.mmu.limit))
        print('baseDir: {var}'.format(var =HARDWARE.mmu.baseDir))

    def free(self, kernel):
        print(HARDWARE.memory.free())

    def clear(self, kernel):
        os.system('clear')

    def kill(self, kernel, comandSplit):
        pid = int(comandSplit[1])
        kernel.closeProcess(pid)
        kernel._pcbTable.removePid(pid)
        print('force kill pid: {}'.format(pid))

    def gannt(self, kernel):
        GANTMODE.modeGant()

    def timeLine(self, kernel, comandSplit):
        temp = comandSplit.pop(0)
        GANTMODE.timeLine(int(comandSplit.pop(0)))

    def nano(self, kernel, comandSplit):
        temp = comandSplit.pop(0)
        os.system('clear')
        kernel.generateProgram(comandSplit.pop(0))

    def info(self, kernel):
        info = kernel.info()

        termcolor.cprint("------------------------------------  :INFORMACION:  ------------------------------------",'red')
        termcolor.cprint("* User:       {}".format(info[0]), 'red')
        termcolor.cprint("* OS:         TpSistemas", 'red')
        termcolor.cprint("* Kernel:     v6.7", 'red')
        termcolor.cprint("* CPU:        Intle Corr j3-4030U @ 1x 1Hz [47.5°C]", 'red')
        termcolor.cprint("* RAM:        {}".format(HARDWARE.memory.size), 'red')
        termcolor.cprint("* FrameSize:  {}".format(info[3]), 'red')
        schedulersDict = {'0': 'SchedulerFCFS',
                          '1': 'SchedulerRR',
                          '2': 'SchedulerByLvlNonPreemptive',
                          '3': 'SchedulerByLvlPreemptive'}
        termcolor.cprint("* Scheduler:  {}".format(schedulersDict[info[1]]), 'red')
        if info[1] == '1': termcolor.cprint("* Quantum:    {}".format(info[2]), 'red')
        termcolor.cprint("------------------------------------  :::::::::::::  ------------------------------------",'red')

CONSOLE = Console()