# Lo que hace cada algoritmo es elegir la victima segun corresponda a la pagetable, y devuelve un diccionario con pid, page y frame.
class AbstractAlgorithm:
    def extractFields(self, row):
        dictToReturn = dict()
        dictToReturn['pid'] = row['pid']
        dictToReturn['page'] = row['page']
        dictToReturn['frame'] = row['frame']
        return dictToReturn

class AlgoritmoFIFO(AbstractAlgorithm):

    def getVictima(self,pageTable):
        row = pageTable.pageTable[0]
        return self.extractFields(row)

class AlgoritmoLRU(AbstractAlgorithm):

    def getVictima(self,pageTable):
        row = pageTable.searchLRU()
        return self.extractFields(row)

class AlgoritmoRSC(AbstractAlgorithm):

    def getVictima(self,pageTable):
        row = pageTable.searchInvalidBitOrReplace()
        return self.extractFields(row)

ALGORITMO = AlgoritmoRSC()