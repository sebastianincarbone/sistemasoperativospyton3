#!/usr/bin/env python
from hardware                     import *
from console                      import *
from interruptions                import *
from scheduler                    import *
from algoritmoDeSelecionDeVictima import *
from tabulate                     import tabulate
from termcolor                    import cprint


class MemoryManager:
    def __init__(self,kernel, frameSize):
        self.__kernel = kernel
        self.__frameSize = frameSize
        self.__freeFrame = list()
        cantidadDeFramesDisponibles = int(HARDWARE.memory.size / frameSize)
        for i in range(0, cantidadDeFramesDisponibles):
            self.__freeFrame.append(i)
        self.__pageTable = PageTable()

    @property
    def kernel(self):
        return self.__kernel

    @property
    def freeFrame(self):
        return self.__freeFrame

    @property
    def pageTable(self):
        return self.__pageTable

    @property
    def frameSize(self):
        return self.__frameSize

    def getFrame(self,pid,page):

        if self.freeFrame:
            frame = self.freeFrame.pop(0)
            self.pageTable.setFrame(pid, page, frame)
        else:
            swapAddress = HARDWARE.hardDisk.getFreeSwapFrame()
            row = ALGORITMO.getVictima(self.pageTable)
            log.logger.info('----:::: Victima ::: -----')
            log.logger.info("** {}".format(row))
            frame = row['frame']
            # le paso el frame en el que buscar la pagina, el loader ira a memoria, la buscará y la llevará a swap
            self.kernel.loader.swapIn(swapAddress, frame)
            self.pageTable.setSwap(row['pid'], row['page'], swapAddress)
            victimPid = row['pid']
            runningPid = self.kernel.pcbTable.runningPid
            if victimPid==runningPid:
                self.kernel.dispatcher.setPageAsSwapInMMU(row['page'])
        return frame

    def getPlaceOf(self, pcb):
        address = self.pageTable.searchSwap(pcb.pid, pcb.pageToLoad)
        return address

    def searchFrame(self,pid,pageForSearch):
        return self.pageTable.searchFrame(pid, pageForSearch)

    def addRow(self,pid,page):
        self.pageTable.addRow(pid,page)

    def update(self, pid):
        listaDefilas = HARDWARE.mmu.pageTable
        for row in listaDefilas:
            self.pageTable.update(pid, row['page'], row['frame'], row['tick'])

    def updateFrame(self, pid, page, frame):
        self.pageTable.updateFrame(pid, page, frame)

    def removeWithPidAndUpdateFreeFrames(self, pid):
        framesList = self.freeFrame + self.pageTable.removeAllAppearances(pid)
        self.__freeFrame = framesList

    def unlockUnusedFrames(self,pcb):
        pidToRemove = pcb.pid
        self.removeWithPidAndUpdateFreeFrames(pidToRemove)

class PCB:
    def __init__(self, pid, nombre, lvl, pageToLoad, pc, estado='ready'):
        self.__pid          = pid
        self.__nombre       = nombre
        self.__lvl          = lvl
        self.__pageToLoad   = pageToLoad
        self.__age          = 0
        self.__pc           = pc
        self.__state        = estado

    @property
    def pid(self):
        return self.__pid

    @property
    def nombre(self):
        return self.__nombre

    @property
    def lvl(self):
        return self.__lvl

    @property
    def pageToLoad(self):
        return self.__pageToLoad

    @property
    def age(self):
        return self.__age

    @property
    def pc(self):
        return self.__pc

    @property
    def state(self):
        return self.__state

    @lvl.setter
    def lvl(self, lvl):
        self.__lvl = lvl

    @pc.setter
    def pc(self, pc):
        self.__pc = pc

    @state.setter
    def state(self, estado):
        self.__state = estado

    @age.setter
    def age(self, age):
        self.__age = age

    def modifyPC(self, new_pc):
        self.pc = new_pc
        return self

    def modifyState(self, state):
        self.state = state
        return self

    def updatePageToLoad(self):
        self.__pageToLoad += 1

class PCBTable:

    def __init__(self):
        self.__listPCB = dict()
        self.__pid = 0
        self.__runningPid = -1
        self.__waitingQueue = list()

    @property
    def listPCB(self):
        return self.__listPCB

    @property
    def waitingQueue(self):
        return self.__waitingQueue

    @property
    def runningPid(self):
        return self.__runningPid

    @runningPid.setter
    def runningPid(self, runningPidNew):
        self.__runningPid = runningPidNew

    @property
    def pid(self):
        return self.__pid

    @pid.setter
    def pid(self, pidNew):
        self.__pid = pidNew

    def isAnyRunning(self):
        return self.runningPid != -1

    def returnRunningPCB(self):
        pid = self.runningPid
        return self.findPcb(pid)

    def addToWaiting (self, pid):
        self.__waitingQueue.append(pid)
        pcb=self.findPcb(pid)
        self.changeState(pcb, 'waiting')

    def findPcb(self, pid):
        return self.listPCB[pid]

    def addPcbToTable(self, pcb):
        self.listPCB[pcb.pid] = pcb

    def generatePID(self, pid):
        return pid + 1

    def updatePCofPcb(self, pcb, pc):
        return pcb.modifyPC(pc)

    def changeState(self, pcb, state):
        estados = {'running': pcb.pid,
                   'waiting': -1,
                   'finished': -1,
                   'ready': -1}
        if self.runningPid == pcb.pid or self.runningPid == -1:
            self.runningPid = estados[state]
        return pcb.modifyState(state)

    def toUpgrade(self):
        retorno = list()
        for k, pcb in self.__listPCB.items():
            if pcb.age >= 3:
                pcb.age = 0
                retorno.append((pcb.age,pcb.lvl))
            else:
                pcb.age = pcb.age + 1

        return retorno

    def __repr__(self):
        retorno = self.listPCB
        if not retorno:
            return 'isEmpty'
        else:
            headers = ['Pid', 'Name', 'lvl', 'pageToLoad', 'pc', 'state']

            data = sorted([(p.pid, p.nombre, p.lvl, p.pageToLoad, p.pc, p.state) for k, p in retorno.items()])

            return tabulate(data, headers=headers, tablefmt="fancy_grid")

class PageTable:
    def __init__(self):
        self.__pageTable = list()

    @property
    def pageTable(self):
        return self.__pageTable

    def setFrame(self,pid,page,frame):
        self.addRow(pid,page,frame)

    def addRow(self, pid, page, frame = -1, tick = -1, inMemory = False, swap = -1, validBit = True):
        newRow = {  'pid'       : pid,
                    'page'      : page,
                    'frame'     : frame,
                    'inMemory'  : inMemory,
                    'swap'      : swap,
                    'validBit'  : validBit,
                    'tick'      : tick}

        self.pageTable.append(newRow)

    def update(self,pid,page,frame,tick):
        for row in self.pageTable:
            if row['pid'] == pid and row['page'] == page:
                row['frame'] = frame
                row['tick '] = tick

    def setSwap(self,pid, page, swapAddress):
        for row in self.pageTable:
            if row['pid'] == pid and row['page'] == page:
                row['swap']  = swapAddress
                row['frame'] = -1

    def __search(self, pid, searchedPage):
        for row in self.pageTable:
            if row['pid'] == pid and row['page'] == searchedPage:
                return row
        return -1

    def searchFrame(self,pid,searchedPage):
        row = self.__search(pid, searchedPage)
        return row['frame']

    def searchLRU(self):
        tick = -1
        rowTemp = -1

        for row in self.pageTable:
            if tick > row['tick'] or rowTemp == -1 :
                tick = row['tick']
                rowTemp = row
        return rowTemp

    def searchSwap(self,pid,searchedPage):
        row = self.__search(pid, searchedPage)
        if row == -1:
            return  row
        return row['swap']

    def updateFrame (self, pid, page, frame):
        for row in self.pageTable:
            if row['pid'] == pid and row['page'] == page:
                row['frame'] = frame
                break

    def searchInvalidBitOrReplace(self):
        rowRet = -1
        for i in range(0, 2):
            if rowRet == -1:
                for row in self.pageTable:
                    if not row['validBit']:
                         rowRet = row
                         break
                    else:
                        row['validBit'] = False
        return rowRet

    def removeAllAppearances(self, pid):
        unlockedFrames = list()
        for r in self.pageTable:
            if r['pid'] == pid:
                unlockedFrames.append(r['frame'])
                self.pageTable.remove(r)
        return unlockedFrames

class Dispatcher:
    def __init__(self, kernel, pcbTable):
        self.__cpu = HARDWARE.cpu
        self.__mmu = HARDWARE.mmu
        self.__kernel = kernel
        self.__pcbTable = pcbTable

    @property
    def cpu(self):
        return self.__cpu

    @property
    def mmu(self):
        return self.__mmu

    @property
    def kernel(self):
        return self.__kernel

    @property
    def pcbTable(self):
        return self.__pcbTable

    #private
    def __loadPagesInMMU(self, pcb):
        progName = '/home/' + pcb.nombre + '.exe'
        programInPages = HARDWARE.hardDisk.getProgram(progName)
        cantidadDePage = len(programInPages)
        for i in range(0, cantidadDePage):  # Armo la nueva tabla de paginas del mmu
            self.mmu.receive(i)

    def load(self, pid):

        loadablePCB=self.pcbTable.findPcb(pid)# Busco el PCB a ser cargado
        pc = loadablePCB.pc
        self.mmu.deletePageTable()  # Borro la antigua tabla de paginas del mmu
        self.cpu.setPC(pc)  # Seteo el PC del CPU igual que el del loadablePCB
        self.pcbTable.changeState(loadablePCB, 'running')  # Actualizo el estado, seteo el runningPid

        self.__loadPagesInMMU(loadablePCB)  # Cargo las paginas en el MMU
        #TODO Flag sobre el pid que se larga a correr
        log.logger.info('-----> Running pid :{}'.format(pid))

    def save(self, pcb):
        self.pcbTable.updatePCofPcb(pcb, self.cpu.pc)
        self.kernel.mmp.update(self.pcbTable.runningPid)

    def changeState(self, pcb, state):
        self.pcbTable.changeState(pcb, state)

    def setFrameInMMU(self,pid, page, updatedFrame):

        self.mmu.setFrame(page, updatedFrame)
        self.kernel.mmp.updateFrame(pid, page, updatedFrame)

    def setPCOfCPUAsIdle(self):
        self.cpu.setPC(-1)

    def resetTickCounter(self):
        self.cpu.resetCountTicks()

    def setPageAsSwapInMMU (self, page):
        self.mmu.setFrame(page, -1)

class Loader:
    def __init__(self, kernel):
        self._mmp = kernel.mmp
        self._memory = HARDWARE.memory
        self._hardDisk = HARDWARE.hardDisk
        self._mmu = HARDWARE.mmu
        self._kernel = kernel

    @property
    def mmp(self):
        return self._mmp

    @property
    def mmu(self):
        return self._mmu

    @property
    def memory(self):
        return self._memory

    @property
    def hardDisk(self):
        return self._hardDisk

    @property
    def kernel(self):
        return self._kernel

    #private
    def __load_page(self,page,frame):
        index = (frame * self.mmp.frameSize)
        for instruccion in page:
            HARDWARE.memory.put(index, instruccion)
            index += 1

    def swapIn(self,swapAddress, frameToSwap):
        listOfInstructions = list()
        for i in range (0,4):
            address = frameToSwap * self.memory.frameSize + i
            listOfInstructions.append(self.memory.get(address))
        HARDWARE.hardDisk.swapIn(swapAddress, listOfInstructions)

    def swapOut(self, swapAddress, listOfInstructions):

        #print("---------------De swap sale : {}".format(HARDWARE.hardDisk.swapOut(swapAddress)))
        self.__load_page(HARDWARE.hardDisk.swapOut(swapAddress), listOfInstructions)

    def getFramesOfProgram(self, programName):
        return self.hardDisk.getProgram(programName)

    def getProgram(self, path):
        return self.hardDisk.getProgram(path)

    def loadPage(self, pcb, pageToLoad, frame):

    # Yo ya tengo donde debo cargarlo, ahora debo ver en dónde está la página a ser cargada, si en swap o en disco

        swapAddress = self.mmp.getPlaceOf(pcb)
        if swapAddress == -1:
            progName = '/home/' + pcb.nombre + '.exe'
            programInPages = self.hardDisk.getProgram(progName)
            page = programInPages[pageToLoad]
            self.__load_page(page, frame)

        else:
            self.swapOut(swapAddress, frame)

        self.kernel.dispatcher.setFrameInMMU(pcb.pid, pageToLoad, frame)  # Le digo al dispatcher que actualice los
                                                                          # datos en el MMU y la PageTable
        pcb.updatePageToLoad()

class Console:
    def __init__(self, kernel, console):
        self.__kernel = kernel
        self.console = console
        console.configConsole()

    @property
    def kernel(self):
        return self.__kernel

    def executeComandLine(self, comand):
        self.console.executeCLI(comand, self.kernel)

class Kernel:

    def __init__(self, user, mode, quantum, frameSize):

        schedulersDict           = {  '0':SchedulerFCFS,
                                      '1':SchedulerRR,
                                      '2':SchedulerByLvlNonPreemptive,
                                      '3':SchedulerByLvlPreemptive}

        self._pcbTable           = PCBTable()
        self._scheduler          = schedulersDict[mode](self)
        self._dispatcher         = Dispatcher(self, self._pcbTable)
        self._console            = Console(self, CONSOLE)
        self._mmp                = MemoryManager(self,int(frameSize)) #establecido con un frame size de 4
        self._loader             = Loader(self)

        killHandler              = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        pageFault                = PageFaultInterruptionHandler(self)
        HARDWARE.interruptVector.register(PAGE_FAULT_INTERRUPTION_TYPE, pageFault)

        timeOutHandler           = TimeOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIME_OUT_INTERRUPTION_TYPE, timeOutHandler)

        ioInHandler              = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        cpuHandler               = CpuIntrInterruptionHandler(self)
        HARDWARE.interruptVector.register(CPU_INTERRUPTION_TYPE, cpuHandler)

        ioOutHandler             = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)

        self._newHandler         = NewInterruptionHandler(self)

        self._newPid             = 0
        self._programList        = list()
        self._flag               = 'run'
        self.__user              = user
        self.__mode              = mode
        self.__quantum           = quantum
        self.__frameSize         = frameSize

    @property
    def mmp(self):
        return  self._mmp

    @property
    def pcbTable(self):
        return self._pcbTable

    @property
    def scheduler(self):
        return self._scheduler

    @property
    def programList(self):
        return  self._programList

    @property
    def flag(self):
        return  self._flag

    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    @property
    def loader(self):
        return self._loader

    @property
    def dispatcher(self):
        return self._dispatcher

    @property
    def console(self):
        return self._console

    @property
    def newPid(self):
        return self._newPid

    @newPid.setter
    def newPid(self, number):
        self._newPid = number

    @flag.setter
    def flag(self, bool):
        self._flag = bool

    def newPCB(self, pid, nombre, lvl, pageToLoad, pc, estado):
        return PCB(pid, nombre, lvl, pageToLoad, pc, estado)

    def isAnyRunning(self):
        return self.pcbTable.isAnyRunning()

    def loadProgramList(self,list):
        lvl = 0
        path = ""

        while list:
            path = '/home/' + list.pop(0) + '.exe'
            # evalua que sea realmente un prtLvl en caso de no serlo el valor por defecto es cero
            if list:
                priorityLvl = list.pop(0)
                if priorityLvl[0] == 'p':
                    list.insert(0, priorityLvl)
                else:
                    lvl = priorityLvl

            self.programList.append((path,lvl))

    def getNextProgram(self):
        if self.programList:
            return self.programList.pop(0)

    def startUP(self):
        self.install()
        while 1:
            if (self.flag == 'run'):
                self.console.executeComandLine(input('{}@proyecto-SO:~/home$ '.format(self.__user)))
            else:
                break

    def install(self):
        HARDWARE.hardDisk.installProgram('/home/prg1.exe', [ASM.CPU(2), ASM.IO(), ASM.CPU(3), ASM.IO(), ASM.CPU(2)])
        HARDWARE.hardDisk.installProgram('/home/prg2.exe', [ASM.CPU(4), ASM.IO(), ASM.CPU(1)])
        HARDWARE.hardDisk.installProgram('/home/prg3.exe', [ASM.CPU(17)])
        HARDWARE.hardDisk.installProgram('/home/prg4.exe', [ASM.CPU(3)])

    def updatePid(self):
        self.newPid = self.pcbTable.generatePID(self.newPid)
        return self.newPid

    def generateProgram(self, nombre):
        path = '/home/' + nombre + '.exe'
        instrucciones = self.__generateProgramAux(nombre)
        compilado = self.__compilar(instrucciones)
        os.system('clear')
        if compilado == 'error':
            print("Compilation Error")
        else:
            HARDWARE.hardDisk.installProgram(path, compilado)

    def __generateProgramAux(self, nombre):
        cprint("kernel progMaker                                        {}                                  ".format(nombre), "white", "on_magenta")
        instrucciones = input("")
        return instrucciones

    def __compilar(self, instrucciones):
        listaDeInst = instrucciones.split(' ')
        listaDeRet = list()
        line = 0
        for inst in listaDeInst:
            listaDeRet.append( self.__buildInst(inst, line))
            line += 1
            if self.__buildInst(inst, line) == 'error':
                return  'error'
        return listaDeRet

    def __buildInst(self, inst, line):
        dict = {
            'cpu' : ASM.CPU(1),
            'io'  : ASM.IO(),
            'exit': ''
        }
        try:
            return dict[inst]
        except:
            print("FAIL_______ line:{}".format(line))
            return 'error'

    def info (self):
        return [self.__user, self.__mode, self.__quantum, self.__frameSize]