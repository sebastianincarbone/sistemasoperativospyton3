import threading
from   so         import *
from   hardware   import *
from   log        import *

class Cpu(threading.Thread):
    def run(self):
        HARDWARE.switchOn()
        print('ShutDown {}'.format(self.getName()))

def login(user, password):
    USERS = {'root': 'root', 'seba': 's', 'uriel': 'u'}
    try:
        return (USERS[user] == password)

    except:
        return False

if __name__ == '__main__':

    #configuracion del LOG
    log.setupLogger('cpu.txt')

    ## Threading
    cpu = Cpu(name= 'Thread-{}'.format('CPU'))
    os.system('clear')
    #-------------------config
    termcolor.cprint("------------------------------------  :LOGIN:  ------------------------------------",'red')
    print('\n \n')
    tmp = termcolor.colored("USER :", 'cyan')
    user = input(tmp)
    print('\n')
    tmp = termcolor.colored("PASS :", 'cyan')
    pw = input(tmp)

    if (login(user,pw)):
        os.system('clear')
        termcolor.cprint("------------------------------------  :WELCOME:  ------------------------------------",'red')
        sleep(.7)
        os.system('clear')


        termcolor.cprint("------------------------------------  :CONFIGURACIONES:  ------------------------------------", 'red')

        print(tabulate([[termcolor.colored('0', 'red'),termcolor.colored('FCFS', 'cyan')],
                        [termcolor.colored('1', 'red'),termcolor.colored('RR'  , 'cyan')],
                        [termcolor.colored('2', 'red'),termcolor.colored('LNP' , 'cyan')],
                        [termcolor.colored('3', 'red'),termcolor.colored('LP' , 'cyan')]],
                        ['__',termcolor.colored('scheduler' , 'green')],tablefmt="fancy_grid"))

        scheduler = input('Numero de scheduler [RR]: ')
        if scheduler == '':
            scheduler = '1'

        os.system('clear')
        termcolor.cprint("------------------------------------  :CONFIGURACIONES:  ------------------------------------",'red')

        memoria = input('Tamaño de memoria [16]: ')
        if memoria == '':
            memoria = '16'

        os.system('clear')
        termcolor.cprint("------------------------------------  :CONFIGURACIONES:  ------------------------------------", 'red')
        frameSize = input('FrameSize [4]: ')
        if frameSize == '':
            frameSize = '4'

        os.system('clear')
        if scheduler == '1':
            termcolor.cprint("------------------------------------  :CONFIGURACIONES:  ------------------------------------", 'red')
            quantum = input('quantum [4]: ')
            if quantum == '':
                quantum = '4'
        else:
            quantum = '0'

        os.system('clear')
        #-------------------Start
        for i in range(0, 7):
            for i in range(1,4):
                if i ==1:
                    print('\n \n \n \n ')
                    termcolor.cprint("              |  ",'magenta')
                    sleep(.1)
                    os.system('clear')
                if i ==2:
                    print('\n \n \n \n ')
                    termcolor.cprint("              \  ",'red')
                    sleep(.1)
                    os.system('clear')
                if i ==3:
                    print('\n \n \n \n ')
                    termcolor.cprint("              -  ",'cyan')
                    sleep(.1)
                    os.system('clear')
                if i ==4:
                    print('\n \n \n \n ')
                    termcolor.cprint("              /  ",'green')
                    sleep(.1)
                    os.system('clear')

        os.system('clear')

        HARDWARE.setup(int(memoria), int(frameSize), int(quantum))

        termcolor.cprint("------------------------------------  :INFORMACION:  ------------------------------------",'red')
        termcolor.cprint("* User:       {}".format(user),'red')
        termcolor.cprint("* Memory:     {}".format(memoria),'red')
        termcolor.cprint("* FrameSize:  {}".format(frameSize),'red')

        schedulersDict = {'0': 'SchedulerFCFS',
                          '1': 'SchedulerRR',
                          '2': 'SchedulerByLvlNonPreemptive',
                          '3': 'SchedulerByLvlPreemptive'}

        termcolor.cprint("* Scheduler:  {}".format(schedulersDict[scheduler]),'red')
        if scheduler == '1': termcolor.cprint("* Quantum:    {}".format(quantum),'red')
        termcolor.cprint("------------------------------------  :::::::::::::  ------------------------------------",'red')

        if scheduler == '2'or scheduler == '3':
                 termcolor.cprint("**   El nivel de prioridad se establece con un numero de 0 a 4,siendo cero el mas importante **", 'red')

        cpu.start()

        Kernel(user,scheduler, quantum, frameSize).startUP()

        cpu.join()

    else:
        os.system('clear')
        termcolor.cprint("------------------------------------  ::  ------------------------------------",'cyan')
        termcolor.cprint("*                     Sorry! Your are not allowed.",'red')
        termcolor.cprint("------------------------------------  ::  ------------------------------------", 'cyan')
