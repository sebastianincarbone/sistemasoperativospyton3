import logging

logger = logging.getLogger()

def setupLogger(filePath):
    ## Configure Logger
    #handler = logging.StreamHandler()
    Filehandler = logging.FileHandler(filePath,mode= 'w')

    formatter = logging.Formatter('%(message)s')

    Filehandler.setFormatter(formatter)
    #handler.setFormatter(formatter)

    #logger.addHandler(handler)
    logger.addHandler(Filehandler)

    logger.setLevel(logging.DEBUG)
