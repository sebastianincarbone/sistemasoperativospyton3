#!/usr/bin/env python
from so import *
from hardware import *
from mode_gant import GANTMODE


class AbstractInterruptionHandler:
    def __init__(self, kernel):
        self._kernel = kernel

    @property
    def kernel(self):
        return self._kernel

    def execute(self, irq):
        print('EXECUTE MUST BE OVERRIDEN in class {classname}'.format(classname=self.__class__.__name__))


class IoDeviceController:

    def __init__(self, device):
        self._device = device
        self._waiting_queue = []
        self._currentPCB = None

    def runOperation(self, pcb, instruction):
        pair = {'pcb': pcb, 'instruction': instruction}
        self._waiting_queue.append(pair)
        self.__load_from_waiting_queue_if_apply()

    def getFinishedPCB(self):
        finishedPCB = self._currentPCB
        self._currentPCB = None
        self.__load_from_waiting_queue_if_apply()
        return finishedPCB

    def __load_from_waiting_queue_if_apply(self):
        if (len(self._waiting_queue) > 0) and self._device.is_idle:
            pair = self._waiting_queue.pop()
            pcb = pair['pcb']
            instruction = pair['instruction']
            self._currentPCB = pcb
            self._device.execute(instruction)

    def __repr__(self):
        return "IoDeviceController for {deviceID} running: {currentPCB} waiting: {waiting_queue}".format(deviceID=self._device.deviceId, currentPCB=self._currentPCB, waiting_queue=self._waiting_queue)


class NewInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, prgAndLvl):

        newPid = self.kernel.updatePid()
        path = prgAndLvl[0]
        lvl = prgAndLvl[1]
        prgSize = len(self.kernel.loader.getProgram(path))
        programName = path.split('/')[2].split('.')[0]
        pcb = self.kernel.newPCB(newPid, programName, lvl, 0, 0, 'new')

        self.kernel.pcbTable.addPcbToTable(pcb)  # Lo agrego a la PCBTable

        for page in range(0, prgSize):
            self.kernel.mmp.addRow(newPid, page)

        self.kernel.scheduler.newPCB(pcb)  # Le mando el nuevo pcb al scheduler para que se fije qué hacer, cargar en CPU o guardar en RT

        GANTMODE.startRecord(True)


class IoInInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        operation = irq.parameters
        pcbRun = self.kernel.pcbTable.returnRunningPCB()  # Busco el PCB actualmente corriendo
        pidRun = pcbRun.pid
        self.kernel.pcbTable.addToWaiting(pidRun)
        self.kernel.ioDeviceController.runOperation(pcbRun, operation)  # Lo mando a IO
        self.kernel.dispatcher.save(pcbRun)  # Guardo el estado actual en el CPU y MMU del pcb corriendo
        nextPid = self.kernel.scheduler.getNextPid()  # Busco el pid del siguiente PCB,
        if nextPid > -1:
            self.kernel.dispatcher.load(nextPid) # Si hay alguien más en la ReadyQueue, lo cargo en el CPU
        else:
            self.kernel.dispatcher.setPCOfCPUAsIdle()


class IoOutInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        pcbFromIO = self.kernel.ioDeviceController.getFinishedPCB()
        self.kernel.scheduler.ioOUT(pcbFromIO)


class TimeOutInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):

        self.kernel.scheduler.timeOut()


class PageFaultInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        tmpPc = HARDWARE.cpu.pc
        # HARDWARE.cpu.setPC(-1)  # Lo dejo idle hasta que termine de cargar lo que necesito

        pidRunning = self.kernel.pcbTable.runningPid
        pcb = self.kernel.pcbTable.returnRunningPCB()
        pageToLoad = irq.parameters

        self.kernel.mmp.update(pidRunning)

        # donde lo tengo que cargar
        frame = self.kernel.mmp.getFrame(pidRunning, pageToLoad)
        #todo aca hay algunos flag interesantes, hay que ver
        #print("-------------PID to load = {}".format(pcb.pid))
        #print("-------------Page to load = {}".format(pageToLoad))
        #print("-------------Frame to load = {}".format(frame))
        self.kernel.loader.loadPage(pcb, pageToLoad, frame)
        # HARDWARE.cpu.setPC(tmpPc)


class KillInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        HARDWARE.cpu.setPC(-1) # Lo primero que hago es poner el PC en -1
        pcbToKill = self.kernel.pcbTable.returnRunningPCB()

        self.kernel.pcbTable.changeState(pcbToKill, 'finished')
        self.kernel.mmp.unlockUnusedFrames(pcbToKill)

        if not self.kernel.scheduler.readyTable.isEmpty():
            pidToLoad = self.kernel.scheduler.getNextPid()

            self.kernel.dispatcher.load(pidToLoad)
        else:
            print("---- :::: Sin Procesos ::: -----")


class CpuIntrInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        GANTMODE.saveImg(HARDWARE.memory, self.kernel.pcbTable.listPCB)
