from tabulate import tabulate
from hardware import *

class ReadyTable():
    def __init__(self):
        self._readyTable    = dict()
        self._readyTable['0'] = list()
        self._readyTable['1'] = list()
        self._readyTable['2'] = list()
        self._readyTable['3'] = list()
        self._readyTable['4'] = list()

    @property
    def readyTable(self):
        return self._readyTable

    def add(self, pid, lvl=0):
    # Agrega un nuevo item a la readyTable a partir de pid y nivel. De no especificarse nivel será 0
        nivel = '{}'.format(lvl)
        self._readyTable[nivel].append(pid)

    def remove(self, pid):
        for lvl, rq in self._readyTable.items():
            if rq.count(pid) > 0:
                rq.remove(pid)

    def aumentarPrioridad(self, listaDeTuplas):
        if listaDeTuplas:
            for tupla in listaDeTuplas.items():
                self.add(tupla[0], (tupla[1] + 1))
                self.remove((tupla[0], tupla[1]))

    def nextPID(self):
        temp = -1
        for i in range(0, 5):
            nivel = '{}'.format(i)
            if self._readyTable[nivel]:
                temp = self._readyTable[nivel].pop(0)
                break
        return temp

    def isEmpty(self):
        return not bool(self._readyTable['0'] or self._readyTable['1'] or self._readyTable['2'] or self._readyTable['3'] or self._readyTable['4'])

    def __repr__(self):
        return tabulate(self._readyTable,['RQ1','RQ2','RQ3','RQ4','RQ5'])

class AbstractScheduler:

    def __init__(self,kernel):
        self._kernel = kernel
        self._readyTable = ReadyTable()

    @property
    def readyTable(self):
        return self._readyTable

    @property
    def kernel(self):
        return self._kernel

    def addToReadyTable(self, pid):
        self.readyTable.add(pid)

    def getNextPid(self):
        return self.readyTable.nextPID()

    def timeOut(self):
        pass

    def newPCB(self, pcb):
        if not self.kernel.pcbTable.isAnyRunning():  # Pregunto si hay alguien corriendo
            self.kernel.dispatcher.load(pcb.pid)  # Esta funcion carga el proceso en el CPU, y las paginas en el MMU
        else:
            self.readyTable.add(pcb.pid)  # Solo se agrega a la ready table.

    def ioOUT(self, pcbFromIO):
        if not self.kernel.isAnyRunning():
            pid = pcbFromIO.pid
            # Si no hay nadie corriendo, cargo el programa que acaba de salir de IO
            self.kernel.dispatcher.load(pid)
        else:
            # Si hay alguien corriendo, solo muevo el pcb a ready
            self.readyTable.add(pcbFromIO.pid)
            self.kernel.pcbTable.changeState(pcbFromIO, 'ready')

class SchedulerFCFS(AbstractScheduler):
    pass

class SchedulerRR(AbstractScheduler):

    def newPCB(self, pcb):
        if not self.kernel.pcbTable.isAnyRunning():  # Pregunto si hay alguien corriendo
            self.kernel.dispatcher.load(pcb.pid)  # Esta funcion carga el proceso en el CPU, y las paginas en el MMU
            self.kernel.dispatcher.resetTickCounter()
        else:
            self.readyTable.add(pcb.pid)  # Solo se agrega a la ready table.

    def timeOut(self):
        if self.readyTable.isEmpty():
            pass
        else:
            runningPCB = self.kernel.pcbTable.returnRunningPCB()
            self.kernel.dispatcher.save(runningPCB)
            self.readyTable.add(runningPCB.pid)
            self.kernel.pcbTable.changeState(runningPCB, 'ready')

            pid = self.getNextPid()
            self.kernel.dispatcher.load(pid)
            self.kernel.pcbTable.changeState(self.kernel.pcbTable.findPcb(pid), 'running')

class SchedulerByLvlNonPreemptive(AbstractScheduler):

    def newPCB(self, pcb):
        if not self.kernel.pcbTable.isAnyRunning():  # Pregunto si hay alguien corriendo
            self.kernel.dispatcher.load(pcb.pid)  # Esta funcion carga el proceso en el CPU, y las paginas en el MMU
        else:
            self.readyTable.add(pcb.pid, pcb.lvl)  # Solo se agrega a la ready table.

    def ioOUT(self, pcbFromIO):
    # NOTA: en caso de que haya alguien corriendo, lo dejo, porque este scheduler no es preemptivo
        if not self.kernel.isAnyRunning():
            # Si no hay nadie corriendo cargo el proceso que me llego como parametro
            pid = pcbFromIO.pid
            self.kernel.dispatcher.load(pid)
        else:
            # Solo agrego el PCB que llegó a ready
            self.readyTable.add(pcbFromIO.pid, pcbFromIO.level)
            self.kernel.dispatcher.changeState(pcbFromIO, 'ready')

class SchedulerByLvlPreemptive(AbstractScheduler):

    def newPCB(self, pcb):
        self.chooseWhoWillRun(pcb)


    def ioOUT(self, pcbFromIO):
        self.chooseWhoWillRun(pcbFromIO)


    def chooseWhoWillRun(self,pcb):
        if not self.kernel.isAnyRunning():
            # Si no hay nadie corriendo cargo el proceso que me llego como parametro
            pid = pcb.pid
            self.kernel.dispatcher.load(pid)

        elif (pcb.lvl < self.kernel.pcbTable.returnRunningPCB().lvl):
            runningPCB = self.kernel.pcbTable.returnRunningPCB()
            # Si hay alguien corriendo me fijo quien tiene más prioridad
            self.kernel.dispatcher.save(runningPCB)
            self.kernel.dispatcher.changeState(runningPCB, 'ready')
            self.readyTable.add(runningPCB.pid, runningPCB.lvl)
            self.kernel.dispatcher.load(pcb.pid)
        else:
            # Solo agrego el PCB que llegó a ready
            self.readyTable.add(pcb.pid, pcb.lvl)
            self.kernel.dispatcher.changeState(pcb, 'ready')